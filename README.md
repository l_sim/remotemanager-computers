# remotemanager-computers

This repository stores specifications of the computers used by the L_Sim team
for use with [remotemanager](https://gitlab.com/l_sim/remotemanager). Each
member should maintain their own branch of this project. In that branch,
you can add the computers you use. When you push to the repository, it will
automatically update a pypi package associated with your name. 

## Installation

`remotemanager` is a dependency of this package. Instead of installing
`remotemanager` directly, you can install this package. This can be done by
the following command:
```
pip install \
remotecomputer@git+https://gitlab.com/l_sim/remotemanager-computers/
```
In practice, you probably will want to install a specific branch of this
repository:
```
pip install -I \
remotecomputer@git+https://gitlab.com/l_sim/remotemanager-computers@archer2
```
(`-I` forces a reinstall).
