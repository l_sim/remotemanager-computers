"""
Test importing of the computers. This is just a guide for the branch
developer, it will only print out what is available.
"""
if __name__ == "__main__":
    from os import environ
    from pydoc import render_doc

    branch = environ["CI_COMMIT_BRANCH"]
    rc = __import__("remotecomputer_" + branch)

    print(render_doc(rc))
