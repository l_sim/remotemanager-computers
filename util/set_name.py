"""
Modify the pyproject.toml file based on the name of the branch
"""
if __name__ == "__main__":
    from os import environ
    from tomli import load
    from tomli_w import dump

    branch = environ["CI_COMMIT_BRANCH"]

    with open("pyproject.toml", "rb") as ifile:
        tdata = load(ifile)

    tdata["project"]["name"] = "remotecomputer_" + branch

    with open("pyproject.toml", "wb") as ofile:
        dump(tdata, ofile)
