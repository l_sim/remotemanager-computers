#!/bin/bash

for NOTEBOOK in notebooks/*.ipynb;
  do
    echo executing $NOTEBOOK;
    jupyter nbconvert --execute --to python $NOTEBOOK
done

rm -r notebooks/*.py

