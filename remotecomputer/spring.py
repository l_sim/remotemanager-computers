from remotemanager.connection.computers.base import BaseComputer, \
    required, optional


class Spring(BaseComputer):
    """
    Computer class for the Spring cluster at RIKEN R-CCS.
    """
    def __init__(self, **kwargs):
        from os.path import join

        if 'host' not in kwargs:
            kwargs['host'] = "spring.r-ccs27.riken.jp"

        super().__init__(**kwargs)

        # Base data
        self.module_purge = False
        self.submitter = "qsub"
        self.shebang = "#!/bin/bash"
        self.pragma = "#PBS"

        # Header
        self.queue = required("queue")

        # Parallel
        self.mpi = required("mpi")
        self.omp = required("omp")

        # Environment
        self.conda = optional("")
        self.omp_stacksize = optional("")
        self.path_to_bigdft = optional("", join("/home", "share", "bigdft"))

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        if self.queue.value in ["spring1", "spring2", "winter3"]:
            cpus_per_node = 44
        elif self.queue.value in ["winter1", "winter2"]:
            cpus_per_node = 36
        elif self.queue.value in ["spring3"]:
            cpus_per_node = 48
        else:
            raise ValueError("Invalid queue name")
        nodes = int(max(self.mpi.value * self.omp.value / cpus_per_node, 1))
        ppn = int(self.mpi.value/nodes)

        ncpus = min(self.mpi.value * self.omp.value, cpus_per_node)

        output = []

        # add our new nodes parameter
        output.append(f'{self.pragma} -q {self.queue.value}')  
        output.append(f'{self.pragma} -l nodes={nodes}:ncpus={ncpus}')
        output.append('')

        # Environment Variables
        output.append('. /opt/intel/oneapi/setvars.sh')
        output.append(f'export OMP_NUM_THREADS={self.omp}')
        output.append("source ~/.bashrc")
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        if self.conda.value is not None:
            output.append(f'conda activate {self.conda}')
        if self.omp_stacksize.value is not None:
            output.append(f'export OMP_STACKSIZE={self.omp_stacksize.value}')

        output.append('export BIGDFT_MPIRUN="mpiexec.hydra ' +
                      '-machinefile $PBS_NODEFILE ' +
                      f'-n {self.mpi.value} -perhost {ppn}"')
        output.append('cd $PBS_O_WORKDIR')

        return output


class SpringLogin(BaseComputer):
    """
    Computer class for the login nodes of the Spring cluster at RIKEN R-CCS.
    """
    def __init__(self, **kwargs):
        from os.path import join

        if 'host' not in kwargs:
            kwargs['host'] = "spring.r-ccs27.riken.jp"

        super().__init__(**kwargs)

        # Base Data
        self.module_purge = False
        self.submitter = "bash"
        self.shebang = "#!/bin/bash"
        self.pragma = "#"

        # Environment
        self.conda = optional("")
        self.path_to_bigdft = optional("", join("/home", "share", "bigdft"))

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        output = []

        # Environment Variables
        output.append("source ~/.bashrc")
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        if self.conda.value is not None:
            output.append(f'conda activate {self.conda}')

        output.append('export BIGDFT_MPIRUN=""')

        return output


class SpringGPU(BaseComputer):
    """
    Computer class for the GPU portion of the Spring cluster at RIKEN R-CCS.
    """
    def __init__(self, **kwargs):
        from os.path import join

        if 'host' not in kwargs:
            kwargs['host'] = "spring.r-ccs27.riken.jp"

        super().__init__(**kwargs)

        # Base data
        self.module_purge = False
        self.submitter = "sbatch"
        self.shebang = "#!/bin/bash"
        self.pragma = "#SBATCH"

        # Header
        self.queue = optional("queue", "autumn4")

        # Parallel
        self.mpi = required("mpi")
        self.omp = required("omp")

        # Environment
        self.conda = optional("")
        self.omp_stacksize = optional("")
        self.path_to_bigdft = optional("", join("/home", "share",
                                                "bigdft_cuda"))

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        if self.queue.value in ["autumn4", "autumn5"]:
            cpus_per_node = 32
        else:
            raise ValueError("Invalid queue name")
        nodes = int(max(self.mpi.value * self.omp.value / cpus_per_node, 1))
        ppn = int(self.mpi.value/nodes)

        output = []

        # add our new nodes parameter
        output.append(f'{self.pragma} --partition={self.queue.value}')
        output.append(f'{self.pragma} --nodes={nodes}')
        output.append(f'{self.pragma} --ntasks-per-node={ppn}')
        output.append(f'{self.pragma} --cpus-per-task={self.omp.value}')
        output.append('')

        # Environment Variables
        output.append("source ~/.bashrc")
        output.append('export I_MPI_PMI_LIBRARY='
                      '/usr/lib/x86_64-linux-gnu/libpmi2.so')
        output.append('export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK')
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        if self.conda.value is not None:
            output.append(f'conda activate {self.conda}')
        if self.omp_stacksize.value is not None:
            output.append(f'export OMP_STACKSIZE={self.omp_stacksize.value}')

        output.append('export BIGDFT_MPIRUN="srun --mpi=pmi2"')
        output.append('cd $SLURM_SUBMIT_DIR')

        return output
