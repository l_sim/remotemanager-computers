from remotemanager.connection.computers.base import BaseComputer, \
    required, optional


class Hokusai(BaseComputer):
    """
    Computer class for the Hokusai BigWaterfall computer.
    """
    def __init__(self, **kwargs):
        if 'host' not in kwargs:
            kwargs['host'] = 'hokusai.riken.jp'

        super().__init__(**kwargs)

        # Base data
        self.module_purge = False
        self.submitter = 'pjsub'
        self.shebang = '#!/bin/bash'
        self.pragma = '#PJM'

        # Header
        self.rscunit = optional("", "bwmpc")
        self.rscgrp = optional("", "batch")
        self.elapse = required("elapse")
        self.key = required("key")

        # Parallel
        self.mpi = required("mpi")
        self.omp = required("omp")

        # Environment
        self.conda = optional("")
        self.path_to_bigdft = required("path_to_bigdft")

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        cpus_per_node = 40
        nodes = int(max(self.mpi.value * self.omp.value / cpus_per_node, 1))
        ppn = int(self.mpi.value/nodes)

        vnode_core = min(self.mpi.value * self.omp.value, 40)

        output = []

        # add our new nodes parameter
        output.append(f'{self.pragma} -L rscunit={self.rscunit}')
        output.append(f'{self.pragma} -L rscgrp={self.rscgrp.value}')
        output.append(f'{self.pragma} -L vnode={nodes}')
        output.append(f'{self.pragma} -L vnode-core={vnode_core}')
        output.append(f'{self.pragma} -L elapse={self.elapse.value}')
        output.append(f'{self.pragma} -g {self.key.value}')        
        output.append('#PJM -j')
        output.append('')

        # Environment Variables
        output.append(f'export OMP_NUM_THREADS={self.omp}')
        output.append("source ~/.bashrc")
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        if self.conda.value is not None:
            output.append(f'conda activate {self.conda}')
        output.append('export FUTILE_PROFILING_DEPTH=1')

        output.append('export BIGDFT_MPIRUN="mpirun -np ' +
                      f'{self.mpi} -ppn {ppn}"')

        return output


class HokusaiLogin(BaseComputer):
    """
    Computer class for the login nodes of the Hokusai BigWaterfall computer.
    """
    def __init__(self, **kwargs):
        if 'host' not in kwargs:
            kwargs['host'] = 'hokusai.riken.jp'

        super().__init__(**kwargs)

        # Base Data
        self.module_purge = False
        self.submitter = "bash"
        self.shebang = "#!/bin/bash"
        self.pragma = "#"

        # Environment
        self.conda = optional("")
        self.path_to_bigdft = required("path_to_bigdft")

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        output = []

        # Environment Variables
        output.append("source ~/.bashrc")
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        if self.conda.value is not None:
            output.append(f'conda activate {self.conda}')

        output.append('export BIGDFT_MPIRUN=""')

        return output
