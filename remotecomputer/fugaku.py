from remotemanager.connection.computers.base import BaseComputer, \
    required, optional


class Fugaku(BaseComputer):
    """
    Computer class for the Fugaku supercomputer.

    Note:
        You can get the hashes for spack with `spack find -lx py-numpy`
        and `spack find -lx py-scipy`.
    """
    def __init__(self, **kwargs):
        if 'host' not in kwargs:
            kwargs['host'] = 'fugaku.r-ccs.riken.jp'

        super().__init__(**kwargs)

        # Base data
        self.module_purge = False
        self.submitter = 'pjsub'
        self.shebang = '#!/bin/sh'
        self.pragma = '#PJM'

        # Header
        self.rscunit = optional("", "rscunit_ft01")
        self.rscgrp = optional("", "small")
        self.freq = optional("", "2200")
        self.eco_state = optional("", "2")
        self.elapse = required("elapse")
        self.key = required("key")
        self.nodeshape = optional("", "")

        # Parallel
        self.mpi = required("mpi")
        self.omp = required("omp")

        # Environment
        self.path_to_bigdft = required("path_to_bigdft")

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        cpus_per_node = 48
        nodes = int(max(self.mpi.value * self.omp.value / cpus_per_node, 1))
        ppn = int(self.mpi.value/nodes)

        output = []

        # add our new nodes parameter
        output.append(f'{self.pragma} -L rscunit={self.rscunit}')
        output.append(f'{self.pragma} -L rscgrp={self.rscgrp.value}')
        output.append(f'{self.pragma} -L freq={self.freq.value}' +
                      f',eco_state={self.eco_state.value}')
        if self.nodeshape.value == "":
            output.append(f'{self.pragma} -L node={nodes}')
        else:
            output.append(f'{self.pragma} -L node={nodes}' +
                          f':{self.nodeshape.value}')
        output.append(f'{self.pragma} -L elapse={self.elapse.value}')
        output.append(f'{self.pragma} --mpi "max-proc-per-node={ppn}"')
        output.append(f'{self.pragma} -g {self.key.value}')
        output.append('')

        # Environment Variables
        output.append(f'export OMP_NUM_THREADS={self.omp}')
        output.append(". /vol0004/apps/oss/spack/share/spack/setup-env.sh")
        output.append("spack load /q6rre3p")
        output.append("spack load /7efrdj4")
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        output.append('export FUTILE_PROFILING_DEPTH=1')

        output.append('export BIGDFT_MPIRUN="mpiexec"')

        return output


class FugakuLogin(BaseComputer):
    """
    Computer class for the login nodes of the Spring cluster at RIKEN R-CCS.
    """
    def __init__(self, **kwargs):
        if 'host' not in kwargs:
            kwargs['host'] = 'fugaku.r-ccs.riken.jp'

        super().__init__(**kwargs)

        # Base Data
        self.module_purge = False
        self.submitter = "bash"
        self.shebang = "#!/bin/bash"
        self.pragma = "#"

        # Environment
        self.path_to_bigdft = required("path_to_bigdft")

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        output = []

        # Environment Variables
        output.append("source ~/.bashrc")
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        output.append(". /vol0004/apps/oss/spack/share/spack/setup-env.sh")
        output.append("spack load py-pip@22.2.2%gcc@12.2.0 " +
                      "arch=linux-rhel8-cascadelake")

        output.append('export BIGDFT_MPIRUN=""')

        return output
