import warnings

from remotemanager.connection.computers.base import BaseComputer, \
    required, optional


class Summer(BaseComputer):
    """
    subclassed URL specialising in connecting to the CEA Summer HPC
    """

    def __init__(self, **kwargs):

        warnings.warn("This method of computer creation is outdated, please update "
                      "remotemanager and use the BaseComputer.from_* methods.")

        if kwargs.get('host', None) is not None:
            raise ValueError('cannot change host of dedicated URL')

        kwargs['host'] = 'summer.intra.cea.fr'
        # count of np attribute of pbsnodes
        self._nodes = {8: 60,
                       12: 16,
                       16: 76,
                       32: 52,
                       40: 44}

        super().__init__(**kwargs)

        self.submitter = 'qsub'
        self.shebang = '#!/bin/bash'
        self.pragma = '#PBS'

        self.tasks = required('tasks')
        self.omp = required('omp')
        self.walltime = required('walltime')
        self.mem = optional('mem')
        self.queue = optional('-q', 'short')
        self.name = optional('-N', 'remote_run')
        self.outfile = optional('-o', 'scheduler_stdout')
        self.errfile = optional('-e', 'scheduler_stderr')

        self.modules = ['python3']
        self.extra = None

    @property
    def ssh(self):
        """
        Returns (str):
            modified ssh string for Summer avoiding perl error
        """
        return 'LANG=C ' + super().ssh

    @property
    def postscript(self):

        return f"""
cd $PBS_O_WORKDIR
export OMP_NUM_THREADS={self.omp}
"""

    def resources_block(self, **kwargs):

        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        nodes = int(round(max(self.tasks.value * self.omp.value / 16, 1)))

        ppn = min(16, self.tasks.value * self.omp.value)

        if self.tasks.value * self.omp.value == 1:
            self.queue.value = 'mono'

        options = {'-N': self.name.value,
                   '-q': self.queue.value,
                   '-o': self.outfile.value,
                   '-e': self.errfile.value,
                   '-l': f'nodes={nodes}:'
                         f'ppn={ppn},'
                         f'walltime={self.walltime.value}'}

        if self.mem:
            options['-l'] += f',mem={self.mem.value}'

        return [f'{self.pragma} {k} {v}' for k, v in options.items()]

    def script(self,
               **kwargs) -> str:
        """
        Takes job arguments and produces a valid jobscript

        Returns:
            (str):
                script
        """
        script = [self.shebang]

        script += self.resources_block(**kwargs)
        script += self.modules_block()

        ppn = min(16, self.tasks.value * self.omp.value)
        npn = max(1, ppn // self.omp.value)

        postscript = f"""
cd $PBS_O_WORKDIR
export OMP_NUM_THREADS={self.omp}
export BIGDFT_MPIRUN='mpirun -np {self.tasks} -ppn {npn}'
"""

        script.append(postscript)

        if self.extra is not None:
            script.append(self.extra)

        return '\n'.join(script)
