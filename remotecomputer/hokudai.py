from remotemanager.connection.computers.base import BaseComputer, \
    required, optional


class GrandChariot(BaseComputer):
    """
    Computer class for the grand chariot machine at Hokkaido University.

    For this machine, you need to configure ssh:
    > Host grand.hucc.hokudai.ac.jp
    >   User (your username)
    >   StrictHostKeyChecking no
    >   UserKnownHostsFile /dev/null
    >   LogLevel quiet

    """
    def __init__(self, **kwargs):
        if 'host' not in kwargs:
            kwargs['host'] = 'grand.hucc.hokudai.ac.jp'

        super().__init__(**kwargs)

        # Base data
        self.module_purge = False
        self.submitter = 'pjsub'
        self.shebang = '#!/bin/bash'
        self.pragma = '#PJM'

        # Header
        self.rscgrp = optional("", None)
        self.elapse = required("elapse")
        self.key = required("key")

        # Parallel
        self.mpi = required("mpi")
        self.omp = required("omp")

        # Environment
        self.conda = optional("")
        self.path_to_bigdft = required("path_to_bigdft")

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        cpus_per_node = 40
        nodes = int(max(self.mpi.value * self.omp.value / cpus_per_node, 1))

        group = self.rscgrp.value
        if group is None:
            if nodes <= 4:
                group = "ea"
            elif nodes <= 8:
                group = "sa"
            elif nodes <= 32:
                group = "ma"
            elif nodes <= 256:
                group = "ha"
        output = []

        # add our new nodes parameter
        output.append(f'{self.pragma} -L rscgrp={group}')
        output.append(f'{self.pragma} -L node={nodes}')
        output.append(f'{self.pragma} --mpi proc={self.mpi.value}')
        output.append(f'{self.pragma} -L elapse={self.elapse.value}')
        output.append(f'{self.pragma} -g {self.key.value}')        
        output.append('#PJM -j')
        output.append('')

        # Environment Variables
        output.append(f'export OMP_NUM_THREADS={self.omp}')
        output.append("source ~/.bashrc")
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        if self.conda.value is not None:
            output.append(f'conda activate {self.conda}')
        output.append('export FUTILE_PROFILING_DEPTH=1')

        output.append('export BIGDFT_MPIRUN="mpiexec.hydra -n ' +
                      '${PJM_MPI_PROC}"')

        return output


class GrandChariotLogin(BaseComputer):
    """
    Computer class for the login nodes of the Grant Chariot computer.

    For this machine, you need to configure ssh:
    > Host grand.hucc.hokudai.ac.jp
    >   User (your username)
    >   StrictHostKeyChecking no
    >   UserKnownHostsFile /dev/null
    >   LogLevel quiet
    """
    def __init__(self, **kwargs):
        if 'host' not in kwargs:
            kwargs['host'] = 'grand.hucc.hokudai.ac.jp'

        super().__init__(**kwargs)

        # Base Data
        self.module_purge = False
        self.submitter = "bash"
        self.shebang = "#!/bin/bash"
        self.pragma = "#"

        # Environment
        self.conda = optional("")
        self.path_to_bigdft = required("path_to_bigdft")

    def resources_block(self, **kwargs):
        from os.path import join
        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        output = []

        # Environment Variables
        output.append("source ~/.bashrc")
        bvars = join(self.path_to_bigdft.value, "install", "bin",
                     "bigdftvars.sh")
        output.append(f'source {bvars}')
        if self.conda.value is not None:
            output.append(f'conda activate {self.conda}')

        output.append('export BIGDFT_MPIRUN=""')

        return output
