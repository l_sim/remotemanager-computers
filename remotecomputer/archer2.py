from remotemanager.connection.computers.base import BaseComputer, \
    required, optional


class archer2(BaseComputer):
    """
    example class for connecting to a remote computer using a torque scheduler
    """

    def __init__(self,
                 passfile: str,
                 module_purge=False,
                 **kwargs):

        kwargs['passfile'] = passfile

        if 'host' not in kwargs:
            kwargs['host'] = 'login.archer2.ac.uk'

        super().__init__(**kwargs)

        self.submitter = 'sbatch'
        self.shebang = '#!/bin/bash'
        self.pragma = '#SBATCH'

        self.mpi = required('--ntasks')
        self.omp = required('--cpus-per-task')
        self.walltime = required('--time')
        self.nodes = optional('--nodes', self.calculate_nodes)
        self.jobname = optional('--job-name')
        self.outfile = optional('--output')
        self.errfile = optional('--error')
        self.account = optional('--account')

        self.queue = optional('')
        self.partition = optional('--partition', 'standard')
        self.qos = optional('--qos', 'standard')

        self.module_purge = module_purge

    def calculate_nodes(self):

        self._logger.info(f'calculating nodes for mpi {self.mpi}, '
                          f'omp {self.omp}')

        nodes = max(int(round((self.mpi.value * self.omp.value)/128)), 1)

        return nodes

    def resources_block(self, **kwargs):

        if 'name' in kwargs:
            # Dataset `name` param detected, use as a default
            if not hasattr(self, 'jobname') or 'jobname' not in kwargs:
                kwargs['jobname'] = kwargs.pop('name')

        self.update_resources(**kwargs)

        if not self.valid:
            raise RuntimeError(f'missing required arguments: {self.missing}')

        options = {}
        for k, v in self.argument_dict.items():
            if k in ['queue', 'partition', 'qos']:
                continue
            if v:
                options[v.flag] = v.value

        if not self.partition or not self.qos:
            if not self.queue:
                raise ValueError('please specify partition and qos, or queue')

            options['--partition'] = self.queue.value
            options['--qos'] = self.queue.value

        else:
            options['--partition'] = self.partition.value
            options['--qos'] = self.qos.value

        options['--export'] = 'none'

        self._internal_extra = f'\nexport OMP_NUM_THREADS={self.omp.value}'

        return [f'{self.pragma} {k}={v}'
                for k, v in sorted(options.items()) if v]
